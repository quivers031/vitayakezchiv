//
//  profileVC.swift
//  ViTaYaKezChIv
//
//  Created by ivanwinarto on 28/05/20.
//  Copyright © 2020 Christian. All rights reserved.
//

import UIKit

class profileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ProfilePicture.image = UIImage(named: BuyerPict)
        ProfileName.text = BuyerName
        ProfileTitleInfo.text = "Customer's Information"
        ProfileInfo1.text = BuyerEmail
        ProfileInfo2.text = BuyerPhone
        ProfileInfo3.text = BuyerAddres
        ProfileBuyerCon.alpha = 1
        ProfileSellerCon.alpha = 0
        
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        ProfilePicture.image = UIImage(named: BuyerPict)
        ProfileName.text = BuyerName
        ProfileTitleInfo.text = "Customer's Information"
        ProfileInfo1.text = BuyerEmail
        ProfileInfo2.text = BuyerPhone
        ProfileInfo3.text = BuyerAddres
        ProfileBuyerCon.alpha = 1
        ProfileSellerCon.alpha = 0
        
        
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBOutlet weak var ProfilePicture: UIImageView!
    
    @IBOutlet weak var ProfileName: UILabel!
    
    @IBOutlet weak var ProfileTitleInfo: UILabel!
    
    @IBOutlet weak var ProfileInfo1: UILabel!
    
    @IBOutlet weak var ProfileInfo2: UILabel!
    
    @IBOutlet weak var ProfileInfo3: UILabel!
    
    @IBOutlet weak var ProfileBuyerCon: UIView!
    
    @IBOutlet weak var ProfileSellerCon: UIView!
    
    
    
    var BuyerName = "ivan"
    var BuyerPict = "cust"
    var BuyerEmail = "ivan@apple.com"
    var BuyerPhone = "0987654321"
    var BuyerAddres = "Griya Kebraon Barat 2, blok BI-22 , Surabaya"
    
    var StoreName = "kokopian"
    var StorePict = "store"
    var StoreEmail = "kokopian@apple.com"
    var StorePhone = "0111111111"
    var StoreAddres = "Raya Darmo nomor 69 , Surabaya"
    
    
    
    
  
    
    
    @IBAction func didChangeSegment(_ sender: UISegmentedControl){
    if sender.selectedSegmentIndex == 0{
        ProfilePicture.image = UIImage(named: BuyerPict)
        ProfileName.text = BuyerName
        ProfileTitleInfo.text = "Customer's Information"
        ProfileInfo1.text = BuyerEmail
        ProfileInfo2.text = BuyerPhone
        ProfileInfo3.text = BuyerAddres
        ProfileBuyerCon.alpha = 1
        ProfileSellerCon.alpha = 0
        
        
    }else {
        ProfilePicture.image = UIImage(named: StorePict)
        ProfileName.text = StoreName
        ProfileTitleInfo.text = "Store's Information"
        ProfileInfo1.text = StoreEmail
        ProfileInfo2.text = StorePhone
        ProfileInfo3.text = StoreAddres
        ProfileBuyerCon.alpha = 0
        ProfileSellerCon.alpha = 1
        
        
    }
    }
    
    
    @IBAction func ButtonEditBaru(_ sender: Any) {
        self.performSegue(withIdentifier: "SegueFull", sender: self)
    }
    
    
    
       
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SegueFull"){
            let editkuy = segue.destination as! editVC
            
            editkuy.NamaBuyer = BuyerName
            editkuy.EmailnyaBuyer = BuyerEmail
            editkuy.TelponBuyer = BuyerPhone
            editkuy.AlamatBuyer = BuyerAddres
            
            editkuy.NamaToko = StoreName
            editkuy.EmailnyaToko = StoreEmail
            editkuy.TelponToko = StorePhone
            editkuy.AlamatToko = StoreAddres
            
            
        }
    }
    
    
    @IBAction func unwindScreen(_ sender: UIStoryboardSegue){
      
    }

    
    
}
