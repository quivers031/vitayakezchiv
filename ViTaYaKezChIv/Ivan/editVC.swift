//
//  editVC.swift
//  ViTaYaKezChIv
//
//  Created by ivanwinarto on 29/05/20.
//  Copyright © 2020 Christian. All rights reserved.
//

import UIKit


class editVC: UIViewController {

    
    @IBOutlet weak var EditBuyerName: UITextField!
    
    @IBOutlet weak var EditBuyerEmail: UITextField!
    
    @IBOutlet weak var EditBuyerPhone: UITextField!
    
    @IBOutlet weak var EditBuyerAdd: UITextField!
    
    @IBOutlet weak var EditStoreName: UITextField!
    
    @IBOutlet weak var EditStoreEmail: UITextField!
    
    @IBOutlet weak var EditStorePhone: UITextField!
    
    
    @IBOutlet weak var EditStoreAdd: UITextField!


    var NamaBuyer: String? = ""
    var EmailnyaBuyer: String? = ""
    var TelponBuyer: String? = ""
    var AlamatBuyer: String? = ""
    
    var NamaToko: String? = ""
    var EmailnyaToko: String? = ""
    var TelponToko: String? = ""
    var AlamatToko: String? = ""

    
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(NamaBuyer)
        EditBuyerName.text = NamaBuyer
        EditBuyerEmail.text = EmailnyaBuyer
        EditBuyerPhone.text = TelponBuyer
        EditBuyerAdd.text = AlamatBuyer
        
        EditStoreName.text = NamaToko
        EditStoreEmail.text = EmailnyaToko
        EditStorePhone.text = TelponToko
        EditStoreAdd.text = AlamatToko
       
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
        let updateEdit = segue.destination as! profileVC
        
        updateEdit.BuyerName = EditBuyerName.text!
        updateEdit.BuyerEmail = EditBuyerEmail.text!
        updateEdit.BuyerPhone = EditBuyerPhone.text!
        updateEdit.BuyerAddres = EditBuyerAdd.text!
        
        updateEdit.StoreName = EditStoreName.text!
        updateEdit.StoreEmail = EditStoreEmail.text!
        updateEdit.StorePhone = EditStorePhone.text!
        updateEdit.StoreAddres = EditStoreAdd.text!
        
        
       }
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
